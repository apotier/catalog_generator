import argparse
import logging

from PIL import Image
from os import walk
import os
from collections import namedtuple


# Coordinates for image processing
# Upper left bound to past the image to
UPPER_LEFT_BOUNDARIES_COORDINATES = (600, 535)
# Target X
TARGET_X = 1000

# log file
LOG_FILE='merge_image.log'

# System specific
# TODO find a way to get that from system
DIRECTORY_LEVEL_SEPARATOR = "/"

#===========================================================================================================
# Definitions
#===========================================================================================================
FileName = namedtuple("File",'directory name')


def merge_image(foreground_path, background_path, out_path):
	logging.info("Merging %s with %s into %s" % (background_path, foreground_path, out_path))
	# create output directory if needed
	# Possible race condition here: see https://stackoverflow.com/questions/273192/how-can-i-create-a-directory-if-it-does-not-exist
	dir_name = os.path.dirname(out_path)
	if not os.path.exists(dir_name):
	    os.makedirs(dir_name)
	foreground = Image.open(foreground_path)
	background = Image.open(background_path)
	ratio = float(TARGET_X)/float(background.width)
	# resize foreground
	#foreground = foreground.resize((TARGET_X, int(foreground.height*ratio)), Image.ANTIALIAS)
	logging.debug("tx: %s, ty: %s " % (TARGET_X, int(background.height*ratio)))
	background.paste(foreground, UPPER_LEFT_BOUNDARIES_COORDINATES, foreground)
	#background.thumbnail((TARGET_X, background.height*ratio), Image.ANTIALIAS)
	#background = background.resize((TARGET_X, int(background.height*ratio)), Image.ANTIALIAS)
	background.save(out_path, format="PNG", optimize=True)


def list_images(directory, filetypes=["png"]):
	"""
	returns a list of tuples (directory, filename)
	"""
	f = []
	for (dirpath, dirnames, filenames) in walk(directory):
		for filename in filenames:
			if filename[len(filename)-3:] in filetypes:
				f.append(FileName(dirpath, filename))
			# This makes sure that it stops with only the top directory (to remove when we will want to support file tree)
		break
	return f

# foreground_endof(background).png
# benjam prejent rose.png

def determine_out_file_name(foreground_name, background_name, extension="png", output_directory="."):
	"""
	Determines the color from the background, and creates a name such as:
	foreground color.png
	Make the assumption that the background file name is in french and of the format
	bla [...] bla color.png
	i.e. with spaces and color in the end

	SPEC:
	logo: benjam prejent.png
	background: tee shirt femme rose.png

	out: tshirt femme rose benjam prejent.png
	"""
	foreground_name_no_ext = foreground_name.split(".")[0]
	background_name_no_ext = background_name.split(".")[0]
	#back_split = background_name.split(" ")
	#color=back_split[len(back_split)-1].split(".")[0]
	logging.debug("foreground_name %s, background_name %s, foreground_name_no_ext %s, background_name_no_ext %s" % (foreground_name, background_name, foreground_name_no_ext, background_name_no_ext))
	return output_directory + DIRECTORY_LEVEL_SEPARATOR + background_name_no_ext + " " + foreground_name_no_ext +  "." + extension


def parameters_validation(args):
	if (args.foreground is None or args.background is None or args.out_directory is None):
		print("Not enough arguments to run")
		return False
	return True


def main():
	"""
	Arguments
	"""
	parser = argparse.ArgumentParser(description='Merges images with a foreground image and outputs PNG')
	parser.add_argument('--foreground', help='Path to the foreground image')
	parser.add_argument('--background', help='Path to the background directory')
	parser.add_argument('--out_directory', help='Path to the output directory')
	parser.add_argument('--log_level', help='Logging level, INFO by default', choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"], default="INFO")
	args = parser.parse_args()
	logging.basicConfig(filename=LOG_FILE, level=args.log_level.upper())
	if not parameters_validation(args):
		parser.print_help()
		exit()

    # business code starting from here
	foreground = FileName(os.path.dirname(args.foreground), os.path.basename(args.foreground))
	images = list_images(args.background)
	for image in images:
		merge_image(args.foreground, image.directory + DIRECTORY_LEVEL_SEPARATOR + image.name, determine_out_file_name(foreground.name, image.name, output_directory=args.out_directory))

if __name__ == "__main__":
	main()

